# make_wallpapers
## usage
Script to generate wallpaper files in various standard sizes.  Usage:

  `./make_wallpaper.bash <filename>`

will generate files at UHD, WQHD, and/or vertical WQHD, provided the input file has exactly the right pixel ratio and is big enough.

## dependencies

1. bash
1. ImageMagick

## TODO
1. Add rounding so that it accepts pixel ratios within 0.1%
1. Rewrite in Python
1. Generalize so that it works from a list of standard resolutions (https://en.wikipedia.org/wiki/Display_resolution#/media/File:Vector_Video_Standards8.svg ?)
