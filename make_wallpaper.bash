#!/bin/bash
# figure out which ratio wallpapers to make
filebasename=`basename "$1" ".jpg"`
echo "Processing: $filebasename"
height=`identify -format "%h" "$1"`
width=`identify -format "%w" "$1"`

if [ -z "$height" ] || [ -z "$width" ]
then
    exit 0
fi

# round to three decimal places, to allow for a ratio off by 1 pixel
ratio_9_16=`python3 -c "print(round((($width / $height) * (16/9)),3) == 1)"`
ratio_10_16=`python3 -c "print(round((($width / $height) * (16/10)),3) == 1)"`
ratio_16_9=`python3 -c "print(round((($width / $height) * (9/16)),3) == 1)"`


if [ "$ratio_16_9" == "True" ]
then
    if (( $width >= 3840 ))
    then
        output="3840×2160/""$filebasename""_3840x2160.jpg"
        mkdir -p "3840×2160/"
        convert -resize 3840x "$1" $output
        echo "$filebasename exactly matches 16:9 and is big enough for 3840×2160."
    fi
    if (( $width >= 2560 ))
    then
        output="2560×1440/""$filebasename""_2560x1440.jpg"
        mkdir -p "2560×1440"
        convert -resize 2560x "$1" $output
        echo "$filebasename exactly matches 16:9 and is big enough for 2560×1440."
    fi
fi

if [ "$ratio_9_16" == "True" ]
   then
    if (( $width >= 1440 )); then
        output="1440×2560/""$filebasename""_1440x2560.jpg"
        mkdir -p "1440×2560"
        convert -resize 1440x "$1" $output
        echo "$filebasename exactly matches 9:16 and is big enough for 1440×2560."
    fi
    if (( $width >= 2160 )); then
        output="2160×3840/""$filebasename""_2160x3840.jpg"
        mkdir -p "2160×3840"
        convert -resize 2160x "$1" $output
        echo "$filebasename exactly matches 9:16 and is big enough for 2160×3840."
    fi
fi

if [ "$ratio_10_16" == "True" ]
   then
    if (( $width >= 1600 )); then
        output="1600×2560/""$filebasename""_1600x2560.jpg"
        mkdir -p "1600×2560"
        convert -resize 1600x "$1" $output
        echo "$filebasename exactly matches 10:16 and is big enough for 1600×2560."
    fi
fi
